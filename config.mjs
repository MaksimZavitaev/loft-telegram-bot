import fs from 'fs';
import dotenv from 'dotenv';

if (process.env.NODE_ENV === 'production') {
  dotenv.config();
} else {
  process.env = Object.assign(
    process.env,
    dotenv.parse(fs.readFileSync('.env.dev'))
  );
}

export const NODE_ENV = process.env.NODE_ENV;
export const PORT = 8080;
export const ADMIN_ID = process.env.ADMIN_ID;
export const MONGO_URL = process.env.MONGO_URL;
export const REDIS_URL = process.env.REDIS_URL;
export const BOT_TOKEN = process.env.BOT_TOKEN;
export const BOT_USERNAME = process.env.BOT_USERNAME;
export const USE_LOCAL_SESSION = process.env.LOCAL_SESSION === 'true';
export const YOUTUBE_TOKEN = process.env.YOUTUBE_TOKEN;
export const YOUTUBE_CHANNEL_ID = process.env.YOUTUBE_CHANNEL_ID;
export const VK_TOKEN = process.env.VK_TOKEN;
export const VK_GROUP_ID = process.env.VK_GROUP_ID;
export const POSTGRES_HOST = process.env.POSTGRES_HOST;
export const POSTGRES_DB = process.env.POSTGRES_DB;
export const POSTGRES_USER = process.env.POSTGRES_USER;
export const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD;
