FROM node:alpine

WORKDIR /usr/app

COPY package.json .

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN npm install --only=production --quiet
