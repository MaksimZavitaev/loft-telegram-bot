import Sequelize from 'sequelize';

import User from './models/user';
import Post from './models/post';
import Option from './models/option';
import Teacher from './models/teacher';
import Course from './models/course';
import Lesson from './models/lesson';

import {
  POSTGRES_DB,
  POSTGRES_HOST,
  POSTGRES_PASSWORD,
  POSTGRES_USER
} from '../config';

const db = new Sequelize(POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, {
  host: POSTGRES_HOST,
  dialect: 'postgres'
});

db.import('User', User);
db.import('Post', Post);
db.import('Option', Option);
db.import('Teacher', Teacher);
db.import('Course', Course);
db.import('Lesson', Lesson);

for (const name in db.models) {
  const model = db.models[name];
  if (model.hasOwnProperty('associate')) {
    model.associate(db.models);
  }
}

db.sync();

export default db;
