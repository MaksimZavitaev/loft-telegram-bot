import Stage from 'telegraf/stage';
import wishScene from '../scenes/wish';
import talkingScene from '../scenes/talking';
import chatScene from '../scenes/chat';
import consultationScene from '../scenes/consultation';
import freeCoursesScene from '../scenes/freecourses';
import loftSchoolScene from '../scenes/loftschool';
import publicLessonScene from '../scenes/loftschool/publiclesson';
import reviewsScene from '../scenes/loftschool/reviews';
import aboutUsScene from '../scenes/loftschool/aboutus';
import videoScene from '../scenes/video';
import itScene from '../scenes/it';
import { start } from '../handlers/commands';

const stage = new Stage();

stage.command('start', async (ctx, next) => {
  if (ctx.user) {
    await next();
    return;
  }

  await start(ctx, next);
});

stage.command('wish', ctx => ctx.scene.enter('wish'));

stage.action('TALKING', ctx => ctx.scene.enter('talking'));
stage.action('FREE_COURSES', ctx => ctx.scene.enter('freeCourses'));
stage.action('LOFT_SCHOOL', ctx => ctx.scene.enter('loftSchool'));
stage.action('VIDEO', ctx => ctx.scene.enter('video'));
stage.action('WISH', ctx => ctx.scene.enter('wish'));
stage.action('IT', ctx => ctx.scene.enter('it'));

stage.hears(/\/\w+/, async ctx => {
  await ctx.reply('If you are confused type /help');
});

stage.register(wishScene);
stage.register(talkingScene);
stage.register(chatScene);
stage.register(consultationScene);
stage.register(freeCoursesScene);
stage.register(loftSchoolScene);
stage.register(publicLessonScene);
stage.register(reviewsScene);
stage.register(aboutUsScene);
stage.register(videoScene);
stage.register(itScene);

export default stage.middleware();
