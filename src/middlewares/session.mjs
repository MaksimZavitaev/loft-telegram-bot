import LocalSession from 'telegraf-session-local';
import RedisSession from 'telegraf-session-redis';

import { REDIS_URL, USE_LOCAL_SESSION } from '../../config';

let session;

if (USE_LOCAL_SESSION) {
  session = new LocalSession({ database: 'db.json' });
} else {
  session = new RedisSession({
    store: { url: REDIS_URL }
  });
}

export default session.middleware();
