import db from '../db';

export default async (ctx, next) => {
  const { from, session } = ctx;
  if (ctx.inlineQuery) {
    return next();
  } else if (ctx.callbackQuery) {
    switch (ctx.callbackQuery.data) {
      case 'CANCEL':
      case 'SELECT':
        return next();
      default: // pass
    }
  }

  const name = from.last_name
    ? `${from.first_name} ${from.last_name}`
    : from.first_name;

  const userObj = Object.assign({}, from, { name });

  await db.models.User.findOrCreate({
    where: { id: from.id.toString() },
    defaults: userObj
  }).spread((user, created) => {
    if (!created) ctx.user = user;
  });

  return next();
};
