import express from 'express';
import bodyParser from 'body-parser';

import ApiRouter from './routes/api';

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', ApiRouter);

export default app;
