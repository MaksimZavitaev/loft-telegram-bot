import db from '../../db';

const Option = db.models.Option;

const controller = {};

controller.all = async (req, res, next) => {
  await Option.all().then(options => {
    res.json(options);
  });
};

controller.get = async (req, res, next) => {
  Option.findOne({ where: { id: req.params.id } }).then(option => {
    res.json(option);
  });
};

controller.post = async (req, res, next) => {
  Option.create(req.body).then(option => {
    res.json(option);
  });
};

controller.put = async (req, res, next) => {
  Option.update(req.body, { where: { id: req.params.id } }).then(count => {
    res.json(count);
  });
};

controller.delete = async (req, res, next) => {
  Option.destroy({ where: { id: req.params.id } }).then(count => {
    res.json({ count });
  });
};

export default controller;
