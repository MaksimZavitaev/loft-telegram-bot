import db from '../../db';

const Lesson = db.models.Lesson;
const Course = db.models.Course;
const Teacher = db.models.Teacher;

const controller = {};

controller.all = async (req, res, next) => {
  await Lesson.all({
    include: [{ model: Course }, { model: Teacher }]
  }).then(lessons => {
    res.json(lessons);
  });
};

controller.get = async (req, res, next) => {
  Lesson.findOne({
    include: [{ model: Course }, { model: Teacher }],
    where: { id: req.params.id }
  }).then(lesson => {
    res.json(lesson);
  });
};

controller.post = async (req, res, next) => {
  Lesson.create(req.body).then(lesson => {
    res.json(lesson);
  });
};

controller.put = async (req, res, next) => {
  Lesson.update(req.body, { where: { id: req.params.id } }).then(count => {
    res.json(count);
  });
};

controller.delete = async (req, res, next) => {
  Lesson.destroy({ where: { id: req.params.id } }).then(count => {
    res.json({ count });
  });
};

export default controller;
