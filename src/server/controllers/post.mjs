import db from '../../db';

const Post = db.models.Post;

const controller = {};

controller.all = async (req, res, next) => {
  await Post.all().then(users => {
    res.json(users);
  });
};

controller.get = async (req, res, next) => {
  Post.findOne({ where: { id: req.params.id } }).then(post => {
    res.json(post);
  });
};

controller.post = async (req, res, next) => {
  Post.max('week').then(max => {
    const week = max + 1 || 0;
    Post.create(Object.assign(req.body, { week })).then(post => {
      res.json(post);
    });
  });
};

controller.put = async (req, res, next) => {
  Post.update(req.body, { where: { id: req.params.id } }).then(post => {
    res.json(post);
  });
};

controller.delete = async (req, res, next) => {
  Post.destroy({ where: { id: req.params.id } }).then(count => {
    res.json({ count });
  });
};

export default controller;
