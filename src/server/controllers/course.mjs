import db from '../../db';

const Course = db.models.Course;
const Lesson = db.models.Lesson;

const controller = {};

controller.all = async (req, res, next) => {
  await Course.all({ include: [{ model: Lesson }] }).then(courses => {
    res.json(courses);
  });
};

controller.get = async (req, res, next) => {
  Course.findOne({
    include: [{ model: Lesson }],
    where: { id: req.params.id }
  }).then(course => {
    res.json(course);
  });
};

controller.post = async (req, res, next) => {
  Course.create(req.body).then(course => {
    res.json(course);
  });
};

controller.put = async (req, res, next) => {
  Course.update(req.body, { where: { id: req.params.id } }).then(count => {
    res.json(count);
  });
};

controller.delete = async (req, res, next) => {
  Course.destroy({ where: { id: req.params.id } }).then(count => {
    res.json({ count });
  });
};

export default controller;
