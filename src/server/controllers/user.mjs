import db from '../../db';

const User = db.models.User;

const controller = {};

controller.all = async (req, res, next) => {
  await User.all().then(users => {
    res.json(users);
  });
};

controller.get = async (req, res, next) => {
  User.findOne({ where: { id: req.params.id } }).then(user => {
    res.json(user);
  });
};

controller.post = async (req, res, next) => {
  res.status(405).json({ message: 'Method not implemented yet' });
};

controller.put = async (req, res, next) => {
  res.status(405).json({ message: 'Method not implemented yet' });
};

controller.delete = async (req, res, next) => {
  res.status(405).json({ message: 'Method not implemented yet' });
};

export default controller;
