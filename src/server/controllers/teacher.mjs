import db from '../../db';

const Teacher = db.models.Teacher;
const Lesson = db.models.Lesson;

const controller = {};

controller.all = async (req, res, next) => {
  await Teacher.all({ include: [{ model: Lesson }] }).then(teachers => {
    res.json(teachers);
  });
};

controller.get = async (req, res, next) => {
  Teacher.findOne({
    include: [{ model: Lesson }],
    where: { id: req.params.id }
  }).then(teacher => {
    res.json(teacher);
  });
};

controller.post = async (req, res, next) => {
  Teacher.create(req.body).then(teacher => {
    res.json(teacher);
  });
};

controller.put = async (req, res, next) => {
  Teacher.update(req.body, { where: { id: req.params.id } }).then(count => {
    res.json(count);
  });
};

controller.delete = async (req, res, next) => {
  Teacher.destroy({ where: { id: req.params.id } }).then(count => {
    res.json({ count });
  });
};

export default controller;
