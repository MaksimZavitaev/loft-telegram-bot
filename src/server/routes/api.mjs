import express from 'express';

import UserController from '../controllers/user';
import PostController from '../controllers/post';
import TeacherController from '../controllers/teacher';
import CourseController from '../controllers/course';
import LessonController from '../controllers/lesson';
import OptionController from '../controllers/option';

const router = express.Router();

router
  .route('/users')
  .get(UserController.all)
  .post(UserController.post);

router
  .route('/users/:id(\\d+)')
  .get(UserController.get)
  .put(UserController.put)
  .delete(UserController.delete);

router
  .route('/posts')
  .get(PostController.all)
  .post(PostController.post);

router
  .route('/posts/:id(\\d+)')
  .get(PostController.get)
  .put(PostController.put)
  .delete(PostController.delete);

router
  .route('/teachers')
  .get(TeacherController.all)
  .post(TeacherController.post);

router
  .route('/teachers/:id(\\d+)')
  .get(TeacherController.get)
  .put(TeacherController.put)
  .delete(TeacherController.delete);

router
  .route('/courses')
  .get(CourseController.all)
  .post(CourseController.post);

router
  .route('/courses/:id(\\d+)')
  .get(CourseController.get)
  .put(CourseController.put)
  .delete(CourseController.delete);

router
  .route('/lessons')
  .get(LessonController.all)
  .post(LessonController.post);

router
  .route('/lessons/:id(\\d+)')
  .get(LessonController.get)
  .put(LessonController.put)
  .delete(LessonController.delete);

router
  .route('/options')
  .get(OptionController.all)
  .post(OptionController.post);

router
  .route('/options/:id(\\d+)')
  .get(OptionController.get)
  .put(OptionController.put)
  .delete(OptionController.delete);

export default router;
