import Markup from 'telegraf/markup';
import Extra from 'telegraf/extra';
import Scene from 'telegraf/scenes/base';
import { HOME_BUTTON, genVideoResult } from '../helpers/util';
import { search } from '../helpers/youtube';

const scene = new Scene('video');

scene.enter(async ctx => {
  ctx.editMessageText(
    `Здесь Вы можете получить ссылки на самые популярные видео нашего YouTube канала, а также подписаться на получение случайного видео каждый день.
Выберите один из пунктов или следующим сообщением отправите ключевые слова для поиска`,
    Markup.inlineKeyboard(
      [
        Markup.callbackButton('3 самых популярных видео', 'TOP'),
        Markup.callbackButton('Последнее видео', 'LAST:1'),
        Markup.callbackButton('5 последних видео', 'LAST:5'),
        Markup.callbackButton('Поиск видео', 'FIND'),
        Markup.callbackButton(
          ctx.user.subscribe_posting ? 'Отписаться' : 'Подписаться',
          'SUBSCRIBE'
        ),
        HOME_BUTTON
      ],
      { columns: 1 }
    ).extra()
  );
});

scene.action('TOP', async ctx => {
  const state = Object.assign(ctx.scene.state, {
    q: '',
    maxResults: 3,
    type: 'video',
    order: 'viewCount'
  });

  const data = await search('', 3, state);
  const { msgText, keyboard } = genVideoResult(data);

  await ctx.editMessageText(
    `Наиболее популярные видео на данный момент:
${msgText}`,
    Extra.markdown().markup(keyboard)
  );
});

scene.action(/LAST:\d/, async ctx => {
  const limit = ctx.callbackQuery.data.split(':')[1];
  const state = Object.assign(ctx.scene.state, {
    q: '',
    maxResults: limit,
    type: 'video',
    order: 'date'
  });

  const data = await search('', limit, state);
  const { msgText, keyboard } = genVideoResult(data);

  await ctx.editMessageText(
    `Последние добавленные видео на канале:
${msgText}`,
    Extra.markdown().markup(keyboard)
  );
});

scene.action('FIND', async ctx => {
  ctx.editMessageText(
    'Введите ключевые слова для поиска видео на нашем канале:',
    Markup.inlineKeyboard([Markup.callbackButton('Cancel', 'CANCEL')]).extra()
  );
});

scene.action('SUBSCRIBE', async ctx => {
  ctx.user
    .update({ subscribe_posting: !ctx.user.subscribe_posting })
    .then(() => {
      ctx.scene.enter('video');
    });
});

scene.action(/TO:\w+/, async ctx => {
  const token = ctx.callbackQuery.data.split(':')[1];
  const state = ctx.scene.state;

  const data = await search(
    state.q,
    state.maxResults,
    Object.assign(state, { pageToken: token })
  );
  const { msgText, keyboard } = genVideoResult(data);

  await ctx.editMessageText(msgText, Extra.markdown().markup(keyboard));
});

scene.on('text', async ctx => {
  const q = ctx.message.text;
  const state = Object.assign(ctx.scene.state, {
    q,
    maxResults: 5,
    type: 'video'
  });

  const data = await search(q, 5, state);
  const { msgText, keyboard } = genVideoResult(data);

  await ctx.reply(
    `По Вашему запросу найдены следующие наиболее подходящие видео:
${msgText}`,
    Extra.markdown().markup(keyboard)
  );
});

export default scene;
