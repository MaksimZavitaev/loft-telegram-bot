import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';
import { sendToAdmin } from '../helpers/util';

const scene = new Scene('consultation');

const communications = ['Звонок', 'Телеграм', 'Email', 'Skype'];

const directions = [
  'Интернет-маркетинг',
  'Мобильная разработка',
  'Веб-дизайн',
  'Веб-разработка'
];

const successMessage = `Спасибо за оставленную заявку!
В ближайшее время с Вами свяжется наш специалист и ответит на все Ваши вопросы`;

const directionsKeyboard = [];

directionsKeyboard.push(
  ...directions.map((name, index) =>
    Markup.callbackButton(name, `SELECT_DIRECTION:${index}`)
  ),
  Markup.callbackButton('Назад', 'TALKING'),
  Markup.callbackButton('Главное меню', 'CANCEL')
);

scene.enter(async ctx => {
  await ctx.editMessageText(
    'Выбери направление обучения и получи бесплатную профориентацию нашего специалиста',
    Markup.inlineKeyboard(directionsKeyboard, { columns: 1 }).extra()
  );
});

scene.action(/SELECT_DIRECTION:\d/, async ctx => {
  const index = ctx.callbackQuery.data.split(':')[1];

  ctx.scene.state.direction = directions[index];
  await ctx.editMessageText(
    'Каким образом общение для Вас будет наиболее комфортным?',
    Markup.inlineKeyboard(
      [
        Markup.callbackButton(communications[0], 'SELECT_COMMUNICATION:0'),
        Markup.callbackButton(communications[1], 'SELECT_COMMUNICATION:1'),
        Markup.callbackButton(communications[2], 'SELECT_COMMUNICATION:2'),
        Markup.callbackButton(communications[3], 'SELECT_COMMUNICATION:3'),
        Markup.callbackButton('Назад', 'TALKING'),
        Markup.callbackButton('Главное меню', 'CANCEL')
      ],
      { columns: 1 }
    ).extra()
  );
});

scene.action(/SELECT_COMMUNICATION:\d/, async ctx => {
  const index = ctx.callbackQuery.data.split(':')[1];
  ctx.scene.state.communication = communications[index];

  const fields = ['Номер телефона', 'Ник или номер телефона', 'Email', 'Skype'];

  const message = `Укажите, пожалуйста, Ваши контактные данные для обратной связи:
- Имя;
- ${fields[index]};`;

  await ctx.editMessageText(
    message,
    Markup.inlineKeyboard([Markup.callbackButton('Отменить', 'CANCEL')], {
      columns: 1
    }).extra()
  );
});

scene.on('text', async ctx => {
  console.log(ctx);
});

export default scene;
