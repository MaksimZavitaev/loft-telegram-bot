import Telegram from 'telegraf';
import Scene from 'telegraf/scenes/base';
import { sendToAdmin, GLOBAL_KEYBOARD } from '../helpers/util';

const wishScene = new Scene('wish');

wishScene.enter(async ctx => {
  await ctx.editMessageText(
    'Если у Вас имеются замечания или предложения к разработчику, то я Вас слушаю...',
    Telegram.Markup.inlineKeyboard([
      Telegram.Markup.callbackButton('Cancel', 'CANCEL')
    ]).extra()
  );
});

wishScene.on('text', async ctx => {
  await sendToAdmin(`Сообщение от @${ctx.from.username}: ${ctx.message.text}`);
  await ctx.reply(
    'Спасибо! Мы успешно отправили Ваше сообщение',
    GLOBAL_KEYBOARD
  );
  await ctx.scene.leave();
});

export default wishScene;
