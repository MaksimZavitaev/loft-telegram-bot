import Markup from 'telegraf/markup';
import Extra from 'telegraf/extra';
import Scene from 'telegraf/scenes/base';
import db from '../db';
import {
  GLOBAL_KEYBOARD,
  HOME_BUTTON,
  I_KNOW_MSG,
  MSG_AFTER_POST,
  PROMO_KEYBOARD,
  sendToAdmin
} from '../helpers/util';
import { cancel, promo, request } from '../handlers/actions';

const scene = new Scene('it');

scene.enter(async ctx => {
  ctx.editMessageText(
    'Интересно раз в неделю получать от меня проверенные советы, рекомендации, статьи и видео о том, как сделать карьеру в IT?',
    Markup.inlineKeyboard(
      [
        Markup.callbackButton('ИНТЕРЕСНО', 'SUBSCRIBE'),
        Markup.callbackButton('Я ВСЕ ЗНАЮ', 'I_KNOW'),
        HOME_BUTTON
      ],
      { columns: 1 }
    ).extra()
  );
});

scene.action('SUBSCRIBE', async ctx => {
  await db.models.Post.findOne({ where: { week: 0 } })
    .then((post, reject) => {
      if (!post) reject();
      ctx
        .editMessageText(post.content, Extra.markdown().webPreview(false))
        .then(() => {
          ctx.user
            .update({ subscribe_posting: true, week_of_posting: 1 })
            .then(user => {
              const { text, extra } = MSG_AFTER_POST;
              ctx.reply(text, extra);
            });
        });
    })
    .catch(() => {
      sendToAdmin('Не найдено записи в Posts');
    });
});

scene.action('UNSUBSCRIBE', async ctx => {
  ctx.user.update({ subscribe_posting: false }).then(() => {
    cancel(ctx);
  });
});

scene.action('INTERESTING', async ctx => {
  ctx.editMessageText(
    'Оставь заявку и получи промокод со скидкой на обучение',
    PROMO_KEYBOARD
  );
});

scene.action('I_KNOW', async ctx => {
  await ctx.editMessageText(I_KNOW_MSG, GLOBAL_KEYBOARD);
});

scene.action('PROMO', promo);

scene.action('REQUEST', request);

export default scene;
