import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';
import { sendToAdmin } from '../helpers/util';

const talkingScene = new Scene('talking');

talkingScene.enter(async ctx => {
  await ctx.editMessageText(
    'Выберете пункт меню',
    Markup.inlineKeyboard([
      [Markup.callbackButton('Консультация', 'CONSULTATION')],
      [Markup.callbackButton('Чат и Соцсети', 'CHAT')],
      [Markup.callbackButton('Главное меню', 'CANCEL')]
    ]).extra()
  );
});

talkingScene.action('CONSULTATION', ctx => ctx.scene.enter('consultation'));
talkingScene.action('CHAT', ctx => ctx.scene.enter('chat'));

export default talkingScene;
