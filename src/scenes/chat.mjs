import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';
import { sendToAdmin } from '../helpers/util';

const chatScene = new Scene('chat');

chatScene.enter(async ctx => {
  await ctx.editMessageText(
    'Вступай в наш чат и подписывайся на соцсети, где постоянно кипит общение',
    Markup.inlineKeyboard(
      [
        Markup.urlButton('Телеграм чат', 'https://t.me/loftblog'),
        Markup.urlButton('ВКонтакте', 'https://vk.com/loftblog'),
        Markup.urlButton('Facebook', 'https://www.facebook.com/loftblog'),
        Markup.urlButton('Instagram', 'https://www.instagram.com/loftblog'),
        Markup.urlButton('YouTube', 'https://www.youtube.com/user/loftblog'),
        Markup.callbackButton('Назад', 'TALKING'),
        Markup.callbackButton('Главное меню', 'CANCEL')
      ],
      { columns: 1 }
    ).extra()
  );
});

chatScene.on('text', async ctx => {
  await sendToAdmin(`A wish from @${ctx.from.username}: ${ctx.message.text}`);
  await ctx.reply('Thanks! We have successfully received your wish');
  await ctx.scene.leave();
});

export default chatScene;
