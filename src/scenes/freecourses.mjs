import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';

const scene = new Scene('freeCourses');

const directions = [
  {
    name: 'Интернет-маркетинг',
    link: 'https://loftschool.com/free/digital-marketing/'
  },
  {
    name: 'Мобильная разработка',
    link: 'https://loftschool.com/free/mobile-development/'
  },
  {
    name: 'Веб-дизайн',
    link: 'https://loftschool.com/free/web-design/'
  },
  {
    name: 'Веб-разработка',
    link: 'https://loftschool.com/free/web-development/'
  }
];

const directionsKeyboard = [];

directionsKeyboard.push(
  ...directions.map((item, index) =>
    Markup.callbackButton(item.name, `SELECT_DIRECTION:${index}`)
  ),
  Markup.callbackButton('Я не знаю', 'DONT_KNOW'),
  Markup.callbackButton('Главное меню', 'CANCEL')
);

scene.enter(async ctx => {
  await ctx.editMessageText(
    'Чем ты интересуешься?',
    Markup.inlineKeyboard(directionsKeyboard, { columns: 1 }).extra()
  );
});

scene.action(/SELECT_DIRECTION:\d/, async ctx => {
  const index = ctx.callbackQuery.data.split(':')[1];
  await ctx.editMessageText(
    `Перейди по ссылке, зарегистрируйся на сайте и посмотри бесплатные  уроки в личном кабинете.
${directions[index].link}`,
    Markup.inlineKeyboard([Markup.callbackButton('Главное меню', 'CANCEL')], {
      columns: 1
    }).extra()
  );
});

scene.action('DONT_KNOW', async ctx => {
  await ctx.editMessageText(
    `Перейди по ссылке и оставь заявку на профориентацию
https://loftschool.com/free/career/`,
    Markup.inlineKeyboard([Markup.callbackButton('Главное меню', 'CANCEL')], {
      columns: 1
    }).extra()
  );
});

export default scene;
