import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';
import { HOME_BUTTON } from '../../helpers/util';

const scene = new Scene('reviews');

scene.enter(async ctx => {
  await ctx.editMessageText(
    'Посмотри на Youtube и почитай во ВКонтакте истории успеха наших выпускников',
    Markup.inlineKeyboard(
      [
        Markup.urlButton(
          'YouTube',
          'https://www.youtube.com/channel/UCF53WBUOOyMM-MeQX4wu3ZA/playlists?disable_polymer=1'
        ),
        Markup.urlButton('ВКонтакте', 'https://vk.com/board76297300'),
        HOME_BUTTON
      ],
      { columns: 1 }
    ).extra()
  );
});

export default scene;
