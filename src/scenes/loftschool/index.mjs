import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';
import { HOME_BUTTON } from '../../helpers/util';

const scene = new Scene('loftSchool');

scene.enter(async ctx => {
  await ctx.editMessageText(
    'Для продолжения выберите один из пунктов меню:',
    Markup.inlineKeyboard(
      [
        // Markup.callbackButton('Расписание', 'SCHEDULE'),
        // Markup.callbackButton('Варианты оплаты', 'PAYMENT'),
        Markup.callbackButton('Отзывы', 'REVIEWS'),
        Markup.callbackButton('Открытый урок', 'PUBLIC_LESSONS'),
        Markup.callbackButton('О нас', 'ABOUT_US'),
        HOME_BUTTON
      ],
      { columns: 1 }
    ).extra()
  );
});

// scene.action('SCHEDULE', ctx => ctx.scene.enter('schedule'));
// scene.action('PAYMENT', ctx => ctx.scene.enter('payment'));
scene.action('REVIEWS', ctx => ctx.scene.enter('reviews'));
scene.action('PUBLIC_LESSONS', ctx => ctx.scene.enter('publicLesson'));
scene.action('ABOUT_US', ctx => ctx.scene.enter('aboutUs'));

export default scene;
