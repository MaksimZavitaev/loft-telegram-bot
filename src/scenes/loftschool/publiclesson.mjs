import Markup from 'telegraf/markup';
import Scene from 'telegraf/scenes/base';
import { HOME_BUTTON, GLOBAL_KEYBOARD } from '../../helpers/util';

const scene = new Scene('publicLesson');

scene.enter(async ctx => {
  await ctx.editMessageText(
    'Интересно посмотреть как проходит живое обучение  в школе?',
    Markup.inlineKeyboard(
      [
        Markup.urlButton(
          'Перейти к уроку',
          'https://www.youtube.com/watch?v=nCQgUSn2Kjg&list=PLbZerpEHZ8s0YHIZFHzFqVKvxkRLHvC4N'
        ),
        Markup.callbackButton('Я все знаю', 'I_KNOW'),
        HOME_BUTTON
      ],
      { columns: 1 }
    ).extra()
  );
});

scene.action('I_KNOW', async ctx => {
  await ctx.editMessageText(
    'Школа Loftschool обладает огромнейшей бесплатной базой знаний. Мы создали портал видеоуроков по IT-технологиям https://loftblog.ru/ (с аудиторией больше 200 000), где ты найдешь свыше 1000 наших обучающих роликов и вебинаров. По ним ты можешь уже сейчас  абсолютно бесплатно учиться программированию, разработке, дизайну и интернет-маркетингу!',
    GLOBAL_KEYBOARD
  );
});

export default scene;
