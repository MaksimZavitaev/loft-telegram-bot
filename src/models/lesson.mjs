export default (s, t) => {
  const Lesson = s.define(
    'Lesson',
    {
      title: {
        type: t.STRING,
        allowNull: false
      },
      description: {
        type: t.TEXT,
        allowNull: false
      },
      date_of_event: t.DATE
    },
    {
      timestamps: false,
      indexes: [
        // {
        //   unique: true,
        //   fields: []
        // }
      ]
    }
  );
  Lesson.associate = function(m) {
    m.Lesson.belongsTo(m.Course);
    m.Lesson.belongsTo(m.Teacher);
  };
  return Lesson;
};
