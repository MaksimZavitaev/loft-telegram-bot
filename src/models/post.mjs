export default (s, t) => {
  const Post = s.define(
    'Post',
    {
      title: {
        type: t.STRING,
        allowNull: false
      },
      content: {
        type: t.TEXT,
        allowNull: false
      },
      week: {
        type: t.INTEGER,
        allowNull: false
      }
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['week']
        }
      ]
    }
  );
  Post.associate = function(models) {
    // associations can be defined here
  };
  return Post;
};
