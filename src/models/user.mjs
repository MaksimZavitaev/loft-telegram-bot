export default (s, t) => {
  const User = s.define(
    'User',
    {
      id: {
        type: t.STRING,
        allowNull: false,
        primaryKey: true
      },
      first_name: {
        type: t.STRING,
        allowNull: false
      },
      last_name: {
        type: t.STRING,
        allowNull: false
      },
      is_bot: {
        type: t.BOOLEAN,
        allowNull: false
      },
      is_admin: {
        type: t.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      name: {
        type: t.STRING,
        allowNull: false
      },
      username: {
        type: t.STRING,
        allowNull: false
      },
      language_code: {
        type: t.STRING,
        allowNull: false
      },
      last_activity: t.DATE,
      subscribe_vk: {
        type: t.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      subscribe_posting: {
        type: t.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      week_of_posting: {
        type: t.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      total_requests: {
        type: t.INTEGER,
        allowNull: false,
        defaultValue: 0
      }
    },
    {
      indexes: []
    }
  );
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};
