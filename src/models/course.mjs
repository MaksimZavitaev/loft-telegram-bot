export default (s, t) => {
  const Course = s.define(
    'Course',
    {
      title: {
        type: t.STRING,
        allowNull: false
      },
      description: {
        type: t.TEXT,
        allowNull: false
      },
      price: {
        type: t.DECIMAL(10, 2),
        allowNull: false
      },
      date_of_start: t.DATE,
      date_of_end: t.DATE
    },
    {
      timestamps: false,
      indexes: [
        // {
        //   unique: true,
        //   fields: []
        // }
      ]
    }
  );
  Course.associate = function(m) {
    m.Course.hasMany(m.Lesson);
  };
  return Course;
};
