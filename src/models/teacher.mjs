export default (s, t) => {
  const Teacher = s.define(
    'Teacher',
    {
      first_name: {
        type: t.STRING,
        allowNull: false
      },
      last_name: {
        type: t.STRING,
        allowNull: false
      }
    },
    {
      timestamps: false,
      indexes: [
        // {
        //   unique: true,
        //   fields: []
        // }
      ]
    }
  );
  Teacher.associate = function(m) {
    m.Teacher.hasMany(m.Lesson);
  };
  return Teacher;
};
