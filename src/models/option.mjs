export default (s, t) => {
  const Option = s.define(
    'Option',
    {
      name: {
        type: t.STRING,
        allowNull: false
      },
      val: {
        type: t.STRING,
        allowNull: false
      }
    },
    {
      timestamps: false,
      indexes: [
        {
          unique: true,
          fields: ['name']
        }
      ]
    }
  );
  Option.associate = function(models) {
    // associations can be defined here
  };
  return Option;
};
