import cron from 'cron';
import metasync from 'metasync';
import Markup from 'telegraf/markup';
import Extra from 'telegraf/extra';
import db from '../db';
import bot from '../bot';
import { HOME_BUTTON, MSG_AFTER_POST } from './util';

const jobs = [];
const User = db.models.User;
const Post = db.models.Post;

// Every day at 10, 15 and 22 o'clock
jobs.push(
  new cron.CronJob('0 0 10,15,22 * * *', async () => {
    await User.findAll({ where: { subscribe_youtube: true } }).then(data => {
      const d = new Date();
      console.log(d.toLocaleTimeString());
      console.log(data);
    });
  })
);

// Every 7 days at 12 o'clock, Saturday
jobs.push(
  new cron.CronJob('0 0 12 */7 * 6', async () => {
    await User.findAll({ where: { subscribe_posting: true } }).then(users => {
      metasync.each(users, (user, cb) => {
        const week = user.week_of_posting;
        Post.findOne({ where: { week } }).then(post => {
          bot.telegram
            .sendMessage(user.id, post.text, Extra.markdown().webPreview(false))
            .then(() => {
              const { text, extra } = MSG_AFTER_POST;
              bot.telegram.sendMessage(user.id, text, extra);
              user.update({ week_of_posting: week + 1 }).then(() => {
                cb();
              });
            });
        });
      });
    });
  })
);

metasync.each(jobs, job => job.start());
