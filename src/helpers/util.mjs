import Telegraf from 'telegraf';
import Markup from 'telegraf/markup';
import { ADMIN_ID } from '../../config';
import bot from '../bot';

export const GLOBAL_KEYBOARD = Telegraf.Markup.inlineKeyboard(
  [
    Markup.callbackButton('ВИДЕОУРОКИ И СТАТЬИ', 'VIDEO'),
    Markup.callbackButton('IT', 'IT'),
    Markup.callbackButton('БЕСПЛАТНЫЕ КУРСЫ', 'FREE_COURSES'),
    Markup.callbackButton('ОБЩЕНИЕ', 'TALKING'),
    Markup.callbackButton('LOFTSCHOOL', 'LOFT_SCHOOL'),
    Markup.callbackButton('ОБРАТНАЯ СВЯЗЬ', 'WISH')
  ],
  { columns: 1 }
).extra();

export const HOME_BUTTON = Markup.callbackButton('Главное меню', 'CANCEL');

export const PROMO_KEYBOARD = Markup.inlineKeyboard(
  [
    Markup.callbackButton('ПОЛУЧИТЬ ПРОМОКОД', 'PROMO'),
    Markup.callbackButton('ОСТАВИТЬ ЗАЯВКУ', 'REQUEST'),
    HOME_BUTTON
  ],
  { columns: 1 }
).extra();

export const I_KNOW_MSG =
  'Школа Loftschool обладает огромнейшей бесплатной базой знаний. Мы создали портал видеоуроков по IT-технологиям https://loftblog.ru/ (с аудиторией больше 200 000), где ты найдешь свыше 1000 наших обучающих роликов и вебинаров. По ним ты можешь уже сейчас  абсолютно бесплатно учиться программированию, разработке, дизайну и интернет-маркетингу!';

export const MSG_AFTER_POST = {
  text: 'Хочешь погрузиться еще больше в тему?',
  extra: Telegraf.Markup.inlineKeyboard(
    [
      Markup.callbackButton('ИНТЕРЕСНО', 'INTERESTING'),
      Markup.callbackButton('ОТМЕНИТЬ РАССЫЛКУ', 'UNSUBSCRIBE'),
      HOME_BUTTON
    ],
    { columns: 1 }
  ).extra()
};

export function sendToAdmin(text) {
  return bot.telegram.sendMessage(ADMIN_ID, text);
}

export function genVideoResult(data) {
  let msgText = '';
  const handleButtons = [];
  const pagesButtons = [];

  if (data.prevPageToken) {
    pagesButtons.push(
      Markup.callbackButton('Prev', `TO:${data.prevPageToken}`)
    );
  }
  if (data.nextPageToken) {
    pagesButtons.push(
      Markup.callbackButton('Next', `TO:${data.nextPageToken}`)
    );
  }

  handleButtons.push(
    ...data.items.map((item, i) => {
      msgText = msgText.concat(
        `___${i + 1}___. ***${item.snippet.title}***\n\n`
      );
      return Markup.urlButton(
        (i + 1).toString(),
        `https://www.youtube.com/watch?v=${item.id.videoId}`
      );
    })
  );

  const keyboard = Markup.inlineKeyboard([
    handleButtons,
    pagesButtons,
    [HOME_BUTTON]
  ]);

  return { msgText, keyboard };
}
