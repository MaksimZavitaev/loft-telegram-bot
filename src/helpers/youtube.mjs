import fetch from 'node-fetch';
import qs from 'querystring';
import { YOUTUBE_TOKEN, YOUTUBE_CHANNEL_ID } from '../../config';

function make(endpoint, options = {}) {
  options = Object.assign(
    { key: YOUTUBE_TOKEN, channelId: YOUTUBE_CHANNEL_ID },
    options
  );
  return fetch(
    `https://www.googleapis.com/youtube/v3/${endpoint}?${qs.stringify(options)}`
  )
    .then(result => result.json())
    .then(result => {
      if (result.error) return Promise.reject(result.error);
      return result;
    });
}

export function search(query, limit = 5, options = {}) {
  return make(
    'search',
    Object.assign(options, { q: query, maxResults: limit, part: 'snippet' })
  );
}
