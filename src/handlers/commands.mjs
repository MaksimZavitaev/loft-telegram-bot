import { sendToAdmin, GLOBAL_KEYBOARD } from '../helpers/util';

export async function start(ctx) {
  // await createUserById(ctx.from.id);
  await ctx.reply(
    `Привет, ${
      ctx.from.first_name
    }! Я бот-помощник. Я работаю в школе Loftschool. :)
    
Я помогу тебе не потеряться в мире IT, если ты новичок или прокачать твои навыки, если у тебя уже есть опыт. Посмотри, чем я могу быть полезен.`,
    GLOBAL_KEYBOARD
  );
  // await ctx.scene.enter('auth');
  await sendToAdmin(`We've got a new user! @${ctx.from.username}`);
}
