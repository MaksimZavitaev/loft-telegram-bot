import Extra from 'telegraf/extra';
import Markup from 'telegraf/markup';
import { GLOBAL_KEYBOARD, HOME_BUTTON, PROMO_KEYBOARD } from '../helpers/util';

export async function cancel(ctx) {
  await ctx.scene.leave();
  await ctx.editMessageText('Главное меню', GLOBAL_KEYBOARD);
}

export async function promo(ctx) {
  const promo = 'This is promo'; // TODO Get promo from database
  await ctx.editMessageText(
    `Используй промокод '${promo}' и получи персональную скидку 5%.`,
    PROMO_KEYBOARD
  );
}

export async function request(ctx) {
  await ctx.editMessageText(
    `Оставьте заявку на профориентацию перейдя по ссылке:`,
    Markup.inlineKeyboard(
      [
        Markup.urlButton(
          'ПРОФОРИЕНТАЦИЯ',
          'https://loftschool.com/free/career/'
        ),
        HOME_BUTTON
      ],
      { columns: 1 }
    ).extra()
  );
}

export async function coursesAndProfessions(ctx) {
  await ctx.editMessageText(
    `Профессии:
·         [Веб-разработчик](https://loftschool.com/professions/web-developer/)
·         [Интернет-маркетолог](https://loftschool.com/professions/marketolog/)
·         [Android-разработчик](https://loftschool.com/professions/android/)
·         [JavaScript-разработчик](https://loftschool.com/professions/javascript/)
 
Курсы:
·         [Основы верстки](https://loftschool.com/course/html-css/)
·         [Основы веб-дизайна](https://loftschool.com/course/web-design/)
·         [Веб для начинающих](https://loftschool.com/course/web-beginner/)
·         [Продвинутый веб](https://loftschool.com/course/web-development/)
·         [JavaScript](https://loftschool.com/course/javascript/)
·         [Комплексный курс по PHP](https://loftschool.com/course/php/)
·         [Android: базовый уровень](https://loftschool.com/course/android/)
·         [Android: продвинутый уровень](https://loftschool.com/course/adv-android/)
·         [SEO для всех](https://loftschool.com/course/seo/)
·         [React.js](https://loftschool.com/course/react/)
·         [Node.js: серверный JavaScript](https://loftschool.com/course/nodejs/)
·         [Продвижение сайтов и проектов](https://loftschool.com/course/marketing/)`,
    Extra.markdown()
      .webPreview(false)
      .markup(Markup.inlineKeyboard([HOME_BUTTON], { columns: 1 }))
  );
}
