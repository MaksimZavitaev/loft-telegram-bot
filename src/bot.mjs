import Telegraf from 'telegraf';
import HttpsProxyAgent from 'https-proxy-agent';
import logger from './middlewares/logger';
import asyncer from './middlewares/asyncer';
import user from './middlewares/user';
import session from './middlewares/session';
import { cancel } from './handlers/actions';
import { sendToAdmin } from './helpers/util';
import scenes from './middlewares/scenes';
import { BOT_TOKEN, USE_LOCAL_SESSION, REDIS_URL } from '../config';
import './helpers/scheduler';
import './db';

// const handlers = require('./handlers');
// const middlewares = require('./middlewares');

const bot = new Telegraf(BOT_TOKEN, {
  telegram: {
    webhookReply: false,
    agent:
      process.env.NODE_ENV === 'development'
        ? new HttpsProxyAgent({
            // proxy for russian blocked devs
            host:
              process.env.https_proxy && process.env.https_proxy.split(':')[0],
            port:
              process.env.https_proxy && process.env.https_proxy.split(':')[1],
            secureProxy: true
          })
        : null
  }
});

bot.context.user = null;

setImmediate(async () => {
  try {
    const data = await bot.telegram.getMe();
    bot.options.username = data.username;
  } catch (e) {
    console.error(e);
  }
});

bot.use(asyncer);
bot.use(session);
bot.use(user);
bot.use(logger);
bot.use(scenes);

bot.action('CANCEL', ctx => cancel(ctx));

bot.catch(e => {
  console.error(e);
  sendToAdmin(`Unhandled Bot Error! ${e.message}`);
});

export default bot;
