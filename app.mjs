import { NODE_ENV, BOT_TOKEN, PORT } from './config';
import { sendToAdmin } from './src/helpers/util';
import bot from './src/bot';
import app from './src/server';

app.listen(PORT);

if (NODE_ENV === 'production') {
  bot.startWebhook(`/${BOT_TOKEN}`, null, PORT);
} else {
  bot.startPolling();
}

console.log(`Server is listening to localhost:${PORT}`);

process.on('unhandledRejection', e => {
  console.log(e);
  sendToAdmin(`Unhandled Rejection! ${e.message}`);
});

process.on('uncaughtException', e => {
  console.log(e);
  sendToAdmin(`Uncaught Exception! ${e.message}`);
});
