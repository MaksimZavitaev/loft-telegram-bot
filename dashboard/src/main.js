import '@babel/polyfill';
import Vue from 'vue';
import VueMoment from 'vue-moment';
import VeeValidate from 'vee-validate';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from './axios';

Vue.config.productionTip = false;

Vue.use(VueMoment);
Vue.use(VeeValidate);

new Vue({
  router,
  store,
  axios,
  render: h => h(App)
}).$mount('#app');
