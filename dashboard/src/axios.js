import Vue from 'vue';
import Axios from 'axios';
import VueAxios from 'vue-axios';

const axios = Axios.create({
  baseURL: '/api',
  headers: {
    'Content-Type': 'application/json'
  }
});

Vue.use(VueAxios, axios);

export default VueAxios;
