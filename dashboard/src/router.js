import Vue from 'vue';
import Router from 'vue-router';
import Main from './components/Main';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: '_home',
      redirect: '/home',
      component: Main,
      children: [
        {
          path: '/home',
          name: 'home',
          component: () =>
            import(/* webpackChunkName: "home" */ './views/Home.vue')
        }
      ]
    },
    {
      path: '/users',
      component: Main,
      children: [
        {
          path: '',
          name: 'users',
          component: () =>
            import(/* webpackChunkName: "users" */ './views/Users/Index.vue')
        }
      ]
    },
    {
      path: '/posts',
      component: Main,
      children: [
        {
          path: '',
          name: 'posts',
          component: () =>
            import(/* webpackChunkName: "posts" */ './views/Posts/Index.vue')
        }
      ]
    },
    {
      path: '/teachers',
      component: Main,
      children: [
        {
          path: '',
          name: 'teachers',
          component: () =>
            import(/* webpackChunkName: "posts" */ './views/Teachers/Index.vue')
        }
      ]
    },
    {
      path: '/courses',
      component: Main,
      children: [
        {
          path: '',
          name: 'courses',
          component: () =>
            import(/* webpackChunkName: "posts" */ './views/Courses/Index.vue')
        }
      ]
    },
    {
      path: '/lessons',
      component: Main,
      children: [
        {
          path: '',
          name: 'lessons',
          component: () =>
            import(/* webpackChunkName: "posts" */ './views/Lessons/Index.vue')
        }
      ]
    }
  ]
});
