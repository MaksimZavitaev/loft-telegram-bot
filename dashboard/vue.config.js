module.exports = {
  // proxy API requests to Valet during development
  devServer: {
    proxy: 'http://localhost',
    disableHostCheck: true
  },
  outputDir: '../public'
};
